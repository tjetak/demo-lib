/**
 * TODO(developer): Uncomment these variables before running the sample.
 */
// const topicName = 'YOUR_TOPIC_NAME';
// const data = JSON.stringify({foo: 'bar'});

// Imports the Google Cloud client library
const {PubSub} = require('@google-cloud/pubsub');

// Creates a client; cache this for further use

class PubSubLib {


	static async publishMessage(topicName, data) {
		// Publishes the message as a string, e.g. "Hello, world!" or JSON.stringify(someObject)
		const pubSubClient = new PubSub();
		const dataBuffer = Buffer.from(data);

		try {
			const messageId = await pubSubClient.topic(topicName).publish(dataBuffer);
			console.log(`Message ${messageId} published.`);
			return messageId;
		} catch (error) {
			console.error(`Received error while publishing: ${error.message}`);
			throw new Error(error)
		}
	}
}
module.exports = PubSubLib;